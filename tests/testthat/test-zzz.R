
test_that("options were set",  {
  expect_equal(
    getOption("ZENTRACLOUD_CACHE_MAX_AGE")
    , 7
  )
})

test_that("options were set",  {
  expect_equal(
    getOption("ZENTRACLOUD_CACHE_MAX_SIZE")
    , 500
  )
})
