
# wrong input

test_that("wrong input tokens", {

  expect_error(
    setZentracloudOptions(
      token = 1
    )
    , regexp = "`token` must be a non-empty character string of length one"
  )

  expect_error(
    setZentracloudOptions(
      token =  c("abc", "def")
    )
    , regexp = "`token` must be a non-empty character string of length one"
  )

})


test_that("wrong input domain", {
  expect_error(
    setZentracloudOptions(
      token = "token1"
      , domain = "not_in_list"
    )
    , regexp = "The entered domain is not valid"
  )
})


setZentracloudOptions(
  token = "token"
  , domain = "default"
)

test_that("token was set and can be read", {
  expect_equal(
    zentracloud:::getZentracloudToken()
    , list(domain = "zentracloud.com", token = "token")
  )
})


test_that("all options are listed", {
  expect_length(
    getZentracloudOptions()
    , 5
  )
})
