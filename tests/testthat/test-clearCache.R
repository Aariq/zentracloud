# create fake cache to clear
cache_path = file.path(
  tempdir()
  , "/device_sn/sensor=xyz/Year=2020/Month=12"
)

dir.create(
  cache_path
  , recursive = TRUE
)

# wrong input
test_that("wrong input clearCache", {

  expect_error(
    clearCache(
      cache_dir = "~/blaaa"
    )
    , regexp = "The provided directory does not exist!"
  )

  expect_error(
    clearCache(
      cache_dir = cache_path
      , file_age = 0.5
    )
    , regexp = "must be an integer of length one"
  )


})


df = data.frame("1" = 1, "2" = 2)

arrow::write_dataset(
  df
  , cache_path
)

wf = list.files(cache_path)
pq = list.files(
  cache_path
  , pattern = ".parquet$"
)

test_that("is silent clearing cache", {
  expect_silent(
    clearCache(
      cache_dir = sub("/device.*", "", cache_path)
      , file_age = 0L
    )
  )
})

test_that("parquet is gone", {
  expect_identical(
    list.files(
      cache_path
    )
    , wf[-which(wf == pq)]
  )
})

unlink(cache_path, recursive = TRUE)
