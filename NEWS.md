# zentracloud 0.4.6 (2023-06-19)

#### ✨ features and improvements
  
  * results from `queryDeviceSettings()` are now cached, so successive runs of
    `getReadings()` will be much faster when data is cached (#40)

#### 🐛 bug fixes

  * bugfix when fetching readings and getting `Error: Unable to convert`

# zentracloud 0.4.4 (2023-06-19)

#### ✨ features and improvements

  * include `error_flag` and `error_description` in data (#37)
  * all data column names are now converted to `lower_snake_case`; names are
    constructed as: `measurement.(value|error_flag|error_description)`


# zentracloud 0.4.3 (2023-06-01)

#### ✨ features and improvements

  * remove usage of timezone (retrieved from GPS signal) to enable use of
    package for sensors without that functionality (#35):
    - Now makes use of UTC time stamp returned from the API
    - Column "datetime" in output is now type character


# zentracloud 0.4.2 (2023-04-27)

#### 🐛 bug fixes

  * fix throttling of API calls to `get_readings` endpoint (#38).


# zentracloud 0.4.1 (2023-04-19)

#### ✨ features and improvements

  * custom `print.zentracloudOptions()` method for class `zentracloudOptions`

#### 💬 documentation etc

  * re-knit README.Rmd and vignette


# zentracloud 0.4.0 (2023-04-19)

#### ✨ features and improvements

  * restructure `token` and `domain` handling in options (#27)
    + new functions `setZentracloudOptions()` and `getZentracloudOptions()`
      replacing `setZentracloudToken()`
    + `token` and `domain` are no longer required in `getReadings()`, etc.,
      as they are automatically taken from the global options.
    * custom `print.token()` method for class `token` to keep token
      value hidden.
  * add schema to resolve type matching issues in `readCache()` (#32)
  * `getReadings()`: add option to not use cache directory with
    `ignore_cache = TRUE` (#31)

#### 💬 documentation etc

  * add chunk to vignette about variable attributes (#34)

#### 🍬 miscellaneous

  * add custom headers to API query for tracking R package usage (#28)
  * give some more information to user while waiting for response from the
    API (#29)


# zentracloud 0.3.3 (2023-03-10)

#### ✨ features and improvements

  * add sample dataset for a query to ZENTRA api (`zentra_data`) for use
    in README and vignette (#26)


# zentracloud 0.3.2 (2023-03-09)

#### 🍬 miscellaneous

  * remove `FALSE` suffix from pkg startup message (#25)


# zentracloud 0.3.1 (2023-03-09)

#### 💬 documentation etc

  * add *URL* and *BugReports* to `DESCRIPTION`
    (for use in `pkgdown` rendering the GitLab pages website)
  * fix vignette link in `README.Rmd`


# zentracloud 0.2.0 (2023-03-08)

#### 🍬 miscellaneous

  * polish package for publication at
    [METER Gitlab](https://gitlab.com/meter-group-inc) (#22)


# zentracloud 0.1.3 (2023-03-06)

#### ✨ features and improvements

  * simplify code for `getData` (#21)


# zentracloud 0.1.2 (2023-02-15)

#### ✨ features and improvements

* include port number in sensor name to avoid mixing data of unnamed
  sensors

#### 🍬 miscellaneous

* shorten functions `getSensorSpecs()` & `getData()`
* change exit criteria in `nextPage()`
* editing *.gitlab-ci.yml* to enable caching of package installation.

#### 🐛 bug fixes

* adapt unit test


# zentracloud 0.1.1 (2022-12-01)

#### 🍬 miscellaneous

* make default cache dir dependent on the operating system by using
`tools::R_user_dir(which = "cache")`


# zentracloud 0.1.0 (2022-11-29)

#### 💬 documentation etc

* added vignette
* some documentation updates

#### 🍬 miscellaneous

* `queryDeviceSettings()` now exported


# zentracloud 0.0.11 (2022-11-24)

#### ✨ features and improvements

* new function `readCache()` that reads everything that is saved in the cache
* new function `zentracloudOptions()` for getting and setting options

#### 🍬 miscellaneous

* update `.onAttach()` function


# zentracloud 0.0.10 (2022-11-23)

#### ✨ features and improvements

* rework token functions:
  no longer writes a file, instead sets token in options

#### 🍬 miscellaneous

* create unit tests


# zentracloud 0.0.9 (2022-11-23)

#### ✨ features and improvements

* removed the option to access the API using username and password


# zentracloud 0.0.8 (2022-11-21)

#### ✨ features and improvements

* now writes each page of results to cache separately to avoid memory issues for
large queries
* remove Rcpp dependency


# zentracloud 0.0.7 (2022-11-16)

#### 🍬 miscellaneous

 * now with option to choose cache directory other than default


# zentracloud 0.0.6 (2022-11-16)

# zentracloud 0.0.5 (2022-11-16)

#### 🍬 miscellaneous

* make `clearCache()` work with new folder structure


# zentracloud 0.0.4 (2022-11-16)

#### ✨ features and improvements

* unit and precision are now attributes of the respective value columns
* sensor is attribute of respective list entry
* adapt `getReadings()` to new output format/process

#### 🍬 miscellaneous

* finally delete all old, commented-out code chunks


# zentracloud 0.0.3 (2022-11-14)

#### ✨ features and improvements

* reads cache only once if all relevant data found cached


# zentracloud 0.0.2 (2022-11-10)

#### ✨ features and improvements

* several new functions to interact with API version 3
* transferring old functions to new httr2 package
* now with cache

#### 💬 documentation etc

* updated READ-ME
* no more tokens in code

#### 🍬 miscellaneous

* Added a NEWS.md file to track project progress
