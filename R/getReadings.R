#' Query Data from ZENTRACLOUD API
#'
#' Query data from the ZENTRA Cloud API for a specific device serial number
#' and time period.
#'
#' @details
#'
#' **Options**
#'
#' To use `getReadings()`, valid values for the ZENTRACLOUD options must be
#' set (see `setZentracloudOptions()`). Make sure that the `token` and
#' `domain` match.
#'
#' **Cache**
#'
#' Per default, a cache directory is created at the location specified in the
#' `ZENTRACLOUD_CACHE_DIR` option at first function-call.
#' Here, the data are saved, sorted by device, sensor, year and month.
#' For subsequent calls, the cache is checked for already
#' existing data before the API is called again.
#'
#' @param device_sn `character` device serial number of interest.
#' @param start_time,end_time `character` datetime strings in the format
#'   "YYYY-MM-DD hh:mm:ss" specifying the start/end of the requested time
#'   period. Must be in logger time zone!
#' @param force_api `logical` If `TRUE`, API call is forced irrespective of
#'   existing cached data. Cache directory is updated with fetched API results.
#'   Default `FALSE.`
#' @param ignore_cache `logical` If `TRUE`, directory path set in
#'   `ZENTRACLOUD_CACHE_DIR` is ignored. No data are read or permanently written
#'   to disk. Default `FALSE`.
#'
#' @return
#' A `list` of `data.frames` for the queried time period with columns for
#' datetime and all values. One list entry for each logger.
#' Unit and precision of each value are stored as attributes
#' of the respective columns.
#'
#' @importFrom arrow write_dataset open_dataset
#' @importFrom dplyr bind_rows filter arrange select collect distinct
#' @importFrom lubridate as_datetime round_date
#' @importFrom uuid UUIDgenerate
#'
#' @examples
#' \dontrun{
#' setZentracloudOptions(
#'   token = Sys.getenv("ZENTRACLOUD_TOKEN")
#' )
#'
#' dat = getReadings(
#'  device_sn = "06-01185"
#'  , start_time = "2022-03-20 00:00:00"
#'  , end_time = "2022-03-26 23:59:00"
#' )
#' }
#'
#' @export
getReadings = function(
    device_sn
    , start_time
    , end_time
    , force_api = FALSE
    , ignore_cache = FALSE
) {

  stopifnot(
    "`device_sn` must be 'character' of length one."
    =  {is.character(device_sn) & length(device_sn) == 1L}
  )

  stopifnot(
    "`force_api` must be 'logical' of length one."
    = {is.logical(force_api) & length(force_api) == 1L}
  )

  stopifnot(
    "`ignore_cache` must be 'logical' of length one."
    = {is.logical(ignore_cache) & length(ignore_cache) == 1L}
  )

  # datetime checks
  stopifnot(
    "`start_time` must be 'character' of length one."
    = {is.character(start_time) & length(start_time) == 1L}
  )
  stopifnot(
    "`end_time` must be 'character' of length one."
    = {is.character(end_time) & length(end_time) == 1L}
  )

  # format datetime
  dates = c(
    gsub(" .*", "", start_time)
    , gsub(" .*", "", end_time)
  )

  times = c(
    gsub(".*? ", "", start_time)
    , gsub(".*? ", "", end_time)
  )

  datetime_valid = IsDate(
    dates
    , times
  )

  if (any(!datetime_valid)) {
    stop(
      "Please check format of entered times! Must start with `YYYY-MM-DD`!"
    )
  }

  stopifnot(
    "start_time must be earlier than end_time!"
    = {lubridate::as_datetime(start_time) < lubridate::as_datetime(end_time)}
  )

  # cache
  if (ignore_cache) {
    cache = tempfile("zentracloud_")
    on.exit(unlink(cache, recursive = TRUE))
  } else {
    cache = getOption("ZENTRACLOUD_CACHE_DIR")
  }

  ## wrong path
  dir_fail = function(c) {
    msg = paste(
      sprintf(
        "Cannot create cache directory at %s"
        , cache
      )
      , "Make sure that the option value of `ZENTRACLOUD_CACHE_DIR` is writable."
    )
    stop(
      msg
      , conditionMessage(c)
      , call. = FALSE
    )
  }

  # check if cache dir exists
  if (!dir.exists(cache)) {

    tryCatch(
      dir.create(cache, recursive = TRUE)
      , error = dir_fail
      , warning = dir_fail
    )
  }

  # settings and timezone
  set = queryDeviceSettings(
    device_sn = device_sn
  )

  # force
  if (force_api) {

    dr = path.expand(
      file.path(
        cache
        , device_sn
      )
    )

    # query data
    queryDevice(
      device_sn = device_sn
      , start_time = start_time
      , end_time = end_time
      , dr = dr
    )

    int_times = prepare_int_time(
      start_time = start_time
      , end_time = end_time
      , offsets = set$device$time_settings
    )

    out = openCache(
      dr
      , start_int = int_times[1]
      , end_int = int_times[2]
    )

    nms = names(out)

    out = lapply(
      seq_along(
        out
      )
      , function(x){
        lyr = out[[x]]
        lyr = dplyr::distinct(lyr)
        lyr
      }
    )
    names(out) = nms


  } else {

    # check if dir for device_sn exists/ if it is empty
    if (
      !dir.exists(path.expand(file.path(cache, device_sn))) ||
        length(
          dir(
            path.expand(file.path(cache, device_sn))
            , all.files = TRUE
            )
          ) == 0
    ) {

      # does not exist --> create dir and query data from API to save to dir
      if (!dir.exists(path.expand(file.path(cache, device_sn)))) {
        dir.create(
          path.expand(
            file.path(
              cache
              , device_sn
            )
          )
        )
      }

      dr = path.expand(
        file.path(
          cache
          , device_sn
        )
      )

      # query data
      queryDevice(
        device_sn = device_sn
        , start_time = start_time
        , end_time = end_time
        , dr = dr
      )

      int_times = prepare_int_time(
        start_time = start_time
        , end_time = end_time
        , offsets = set$device$time_settings
      )

      out = openCache(
        dr
        , start_int = int_times[1]
        , end_int = int_times[2]
      )

    } else {

      dr = path.expand(
        file.path(
          cache
          , device_sn
        )
      )

      # find out how many entries there should be
      tdiffs = difftime(
        end_time
        , start_time
        , units = "secs"
      )

      itvl = unique(
        set$device$measurement_settings$measurement_interval_seconds
      )

      mitvl = max(
        itvl
      )


      nrw = tdiffs / mitvl

      #test nrow
      int_times_sup = prepare_int_time(
        start_time = start_time
        , end_time = end_time
        , offsets = set$device$time_settings
      )

      out = openCache(
        dr
        , start_int = int_times_sup[1]
        , end_int = int_times_sup[2]
      )

      nms = names(out)

      out = lapply(
        seq_along(
          out
        )
        , function(x){
          lyr = out[[x]]
          lyr = dplyr::distinct(lyr)
          lyr
        }
      )
      names(out) = nms

      if (length(out) == 0 || nrow(out[[1]]) == 0) {

        # query data
        queryDevice(
          device_sn = device_sn
          , start_time = start_time
          , end_time = end_time
          , dr = dr
        )

      } else {

        nrw_obs = nrow(out[[1]])

        if (abs(nrw - nrw_obs) > 5) {

          start_obs = out[[1]]$timestamp_utc[1]
          end_obs = out[[1]]$timestamp_utc[nrow(out[[1]])]

          # start not the same
          if (!start_obs > int_times_sup[1] - 3600 |
              !start_obs < int_times_sup[1] + 3600) {

            start_obs_char = out[[1]]$datetime[1]
            start_obs_char = substr(start_obs_char, 1, 19)

            queryDevice(
              device_sn = device_sn
              , start_time = start_time
              , end_time = start_obs_char
              , dr = dr
            )
          }

          # end not the same
          if (!end_obs > int_times_sup[2] - 3600 |
              !end_obs < int_times_sup[2] + 3600) {

            end_obs_char = out[[1]]$datetime[nrow(out[[1]])]
            end_obs_char = substr(end_obs_char, 1, 19)

            queryDevice(
              device_sn = device_sn
              , start_time = end_obs_char
              , end_time = end_time
              , dr = dr
            )
          }


          # # hole in the middle
          pos = which(
            !difftime(
              lubridate::as_datetime(out[[1]]$timestamp_utc[-1])
              , lubridate::as_datetime(out[[1]]$timestamp_utc[-length(out[[1]]$timestamp_utc)])
              , units = "secs"
            ) %in% c(itvl, 0)
          )

          if (length(pos) > 0) {

            prt = lapply(
              seq_along(
                pos
              )
              , function(y) {

                st = as.character(
                  out[[1]]$datetime[pos[y]]
                )
                st_char = substr(st, 1, 19)

                nd = as.character(
                  out[[1]]$datetime[pos[y] + 1]
                )
                nd_char = substr(nd, 1, 19)

                queryDevice(
                  device_sn = device_sn
                  , start_time = st_char
                  , end_time = nd_char
                  , dr = dr
                )
              }
            )
          }
        } else {
          return(out)
        }
      }

      out = openCache(
        dr
        , start_int = int_times_sup[1]
        , end_int = int_times_sup[2]
      )

      nms = names(out)

      out = lapply(
        seq_along(
          out
        )
        , function(x){
          lyr = out[[x]]
          lyr = dplyr::distinct(lyr)
          lyr
        }
      )
      names(out) = nms

    }
  }

  return(out)
}

