# Helpers for main function

#' @importFrom jsonlite fromJSON

sendRequest = function(
    token
    , url
) {

  req = httr2::request(url)

  # add authentication
  req = httr2::req_headers(req, "Authorization" = paste("Token", token))

  # add User-Agent header
  req = httr2::req_headers(
    req
    , "X-User-Agent" = "R/zentracloud"
    , "X-Package-Version" = as.character(utils::packageVersion("zentracloud"))
    , "X-Rsession-Id" = Sys.getpid()
    , "X-Os-System" = getOs()
  )

  # specify realm for throttling
  # helps to differentiate between endpoints
  # (and does not throttle requests for complete domain)
  realm = gsub(pattern = "\\?.*$", replacement = "", x = url)

  # throttle for current zentracloud API
  req = httr2::req_throttle(
    req
    , rate = 1 / 62
    , realm = realm
  )

  # extract additional error information
  req = httr2::req_error(
    req
    , body = \(resp) {
      httr2::resp_body_json(resp)$detail
    }
  )

  # perform request
  rsp = httr2::req_perform(req)

  message("[retrieving data]\n")

  # extract response
  txt = httr2::resp_body_string(rsp)

  jsn = jsonlite::fromJSON(txt)

  return(jsn)
}



IsDate = function(
    mydate
    , mytime
) {
  tryCatch(
    !is.na(
      suppressWarnings(
        chron::chron(
          mydate
          , mytime
          , format = c(dates = "y-m-d", times = "h:m:s"))))
    , error = function(err) {FALSE}
  )
}

#' @importFrom arrow open_dataset
#' @importFrom dplyr all_of arrange collect filter select

openCache = function(
    directory
    , start_int
    , end_int
) {

  # global bindings
  timestamp_utc = datetime = NULL

  # read parquet
  pq = arrow::open_dataset(
    directory
    , unify_schemas = TRUE
  )

  fls = pq$files
  fls = lapply(
    seq_along(fls)
    , function(x) {
      fl = sub("/Year.*", "", fls[[x]])
    }
  )

  fls = unique(fls)

  # exclude date month from returned df
  excl = c(
    "Year"
    , "Month"
  )

  dat = lapply(
    seq_along(fls)
    , function(x) {

      pq = arrow::open_dataset(
        fls[[x]]
        , unify_schemas = TRUE
      )

      pq |>
        dplyr::filter(
          timestamp_utc >= start_int &
            timestamp_utc <= end_int
        ) |>
        dplyr::select(-dplyr::all_of(excl)) |>
        dplyr::arrange(datetime) |>
        dplyr::collect()

    }
  )

  nms = sub(".*/sensor=", "", fls)

  out = lapply(
    seq_along(dat)
    , function(x) {
      dat = dat[[x]]
      attr(dat, "sensor") = nms[[x]]
      return(dat)
    }
  )
  names(out) = nms

  return(out)
}


getOs = function(){
  if (.Platform$OS.type == "windows") {
    "win"
  }
  else if (Sys.info()[["sysname"]] == "Darwin") {
    "macos"
  }
  else {
    "other"
  }
}

#' @importFrom lubridate as_datetime
#' @importFrom dplyr mutate filter

prepare_int_time = function(
    start_time
    , end_time
    , offsets
) {

  timezone_offset = valid_since = local_time = NULL

  offsets = offsets |> dplyr::mutate(timezone_offset = timezone_offset/4)

  offsets = offsets |> dplyr::mutate(
    valid_since = lubridate::as_datetime(valid_since)
    , local_time = valid_since + (timezone_offset * 3600)
  )

  start_int = lubridate::as_datetime(
    start_time
  )

  start_offset = offsets |> dplyr::filter(local_time < start_int)
  start_offset = start_offset$timezone_offset[1]

  start_int = as.integer(
    start_int - start_offset * 3600
  )


  end_int = lubridate::as_datetime(
    end_time
  )

  end_offset = offsets |> dplyr::filter(local_time < end_int)
  end_offset = end_offset$timezone_offset[1]

  end_int = as.integer(
    end_int - end_offset * 3600
  )

  return(
    c(start_int, end_int)
    )
}
