# TODO: come back here for simplifying the token stuff
# TODO: we should add a masking to the tokens as well.

#' Write Package Settings to Global Options
#'
#' Five package options are set, see **Arguments**. For retrieving the current
#' set of options use `getZentracloudOptions()`.
#'
#' @details
#' `token`s are only valid for a specific `domain`. Currently, the match is not
#' verified here.
#'
#' @param token valid token for the ZENTRA Cloud API that is to be
#'   saved to the options.
#' @param domain `character` The domain for which the token is valid.
#'   One of "default" (*zentracloud.com*),
#'   "tahmo", "aroya", "skala", "EU", "CN".
#'   Corresponds to the *URL* where the token is created.
#' @param cache_dir directory for setting up the cache. If `NULL`,
#'   default is set depending on the operating system.
#' @param cache_max_age the maximum file age allowed to be cached (in days).
#'   If `NULL`, set to default `7`.
#' @param cache_max_size the maximum cache size (in Kb). If `NULL`, set to
#'   default `500`.
#'
#' @examples
#' setZentracloudOptions(
#'   token = "tkn1!$"
#'   , domain = "default"
#' )
#' str(
#'   getZentracloudOptions()
#' )
#'
#'
#'
#'@name options
NULL

#' @rdname options
#' @export
setZentracloudOptions = function(
    token
    , domain = "default"
    , cache_dir = NULL
    , cache_max_age = NULL
    , cache_max_size = NULL
){

  # check valid token
  if (!is.character(token) || length(token) != 1L || !nzchar(token)) {
    stop(
      "`token` must be a non-empty character string of length one."
      , call. = FALSE
    )
  }

  # use right domain
  dmn = switch(
    domain
    , "default" = "zentracloud.com"
    , "EU" = "zentracloud.eu"
    , "CN" = "zentracloud.cn"
    , "aroya" = "aroya.zentracloud.com"
    , "tahmo" = "tahmo.zentracloud.com"
    , "skala" = "zl6.skalacontrol.com"
    , stop(
      paste(
        "The entered domain is not valid.",
        "Please refer to the list of possibilities in the help."
      )
      , call. = FALSE
    )
  )

  # TODO: check with ping endpoint eventually? Currently not available

  # set global options
  options("ZENTRACLOUD_TOKEN" = token)
  options("ZENTRACLOUD_DOMAIN" = dmn)

  if (is.null(cache_dir)) {
    cache_dir = .zentraCloudOptionsDefaults()$ZENTRACLOUD_CACHE_DIR
  }
  options("ZENTRACLOUD_CACHE_DIR" = cache_dir)

  if (is.null(cache_max_age)) {
    cache_max_age = .zentraCloudOptionsDefaults()$ZENTRACLOUD_CACHE_MAX_AGE
  }
  stopifnot("`cache_max_age` must be integer" = {is.integer(cache_max_age)})
  options("ZENTRACLOUD_CACHE_MAX_AGE" = cache_max_age)

  if (is.null(cache_max_size)) {
    cache_max_size = .zentraCloudOptionsDefaults()$ZENTRACLOUD_CACHE_MAX_SIZE
  }
  stopifnot("`cache_max_size` must be integer" = {is.integer(cache_max_size)})
  options("ZENTRACLOUD_CACHE_MAX_SIZE" = cache_max_size)

  invisible(NULL)
}

#' @rdname options
#' @export
getZentracloudOptions = function() {
  opt = options()
  opt = opt[grep(pattern = "ZENTRACLOUD", x = names(opt))]

  # required options
  req = paste0(
    "ZENTRACLOUD_"
    , c("CACHE_DIR", "CACHE_MAX_AGE", "CACHE_MAX_SIZE", "DOMAIN", "TOKEN")
  )

  # check if any missing
  miss = setdiff(req, names(opt))

  if (length(miss) == 0) {

    # for customized printing
    opt[["ZENTRACLOUD_TOKEN"]] = structure(
      opt[["ZENTRACLOUD_TOKEN"]]
      , class = c("token", "character")
    )

    opt = structure(opt, class = c("zentracloudOptions", "list"))

    return(opt)
  }

  msg = paste(
    sprintf("Some required options not set: %s", paste(miss, collapse = ", "))
    , "Use `setZentracloudOptions()` for setting all options properly."
    , sep = "\n"
    )

  stop(msg, call. = FALSE)
}



# helper functions -------------------------------------------------------------


#' Read token from options
#'
#' @description
#' Reads the token and domain from the options, for use in the R session.
#' Make sure a token is set, this can be done using [setZentracloudOptions()].
#' @return
#' A list of length 2, containing the domain and the corresponding token.
#' @noRd
#'
getZentracloudToken = function() {

  tk = getOption("ZENTRACLOUD_TOKEN")

  stopifnot("No token found in options. Set one using `setZentracloudOptions()`"
            = {!is.null(tk)}
  )

  stopifnot(
    "Retrieved token is an empty string.
    Please set a valid token using `setZentracloudOptions()`"
    = {nchar(tk) > 1}
  )

  dmn = getOption("ZENTRACLOUD_DOMAIN")

  stopifnot("No domain found in options. Set one using `setZentracloudOptions()`"
            = {!is.null(dmn)}
  )

  return(list("domain" = dmn, "token" = tk))
}


# default settings for the zentracloud cache
#' @importFrom tools R_user_dir
.zentraCloudOptionsDefaults = function() {
  list(
    "ZENTRACLOUD_CACHE_DIR" = tools::R_user_dir("zentracloud", which = "cache")
    , "ZENTRACLOUD_CACHE_MAX_SIZE" = 500L
    , "ZENTRACLOUD_CACHE_MAX_AGE" = 7L
  )
}


#' @export
print.token = function(x, ...) {
  cat("<--hidden-->")
  invisible(x)
}


#' @export
print.zentracloudOptions = function(x, header = TRUE, ...) {
  cat(format(x, header = header, ...), sep = "\n")
  invisible(x)
}

#' @export
format.zentracloudOptions = function(x, header = TRUE, ...) {

  nms = names(x)
  x = do.call("c", args = x)

  x[which(nms == "ZENTRACLOUD_TOKEN")] = "<-- hidden -->"

  c(
    if (header) "<zentracloudOptions>",
    paste0("  ", format(nms), ": ", x)
  )
}
