#' Clear Zentracloud Package Cache
#'
#' Clear package cache of files older then the specified number of days.
#'
#' @details
#' `clearCache` is automatically called when the package is attached.
#'
#' @param cache_dir `character` Cache folder. Defaults to the cache directory
#'   stored in options, but can be changed if files were saved somewhere else.
#'   MUST be the highest level of the cache to run properly.
#' @param file_age `integer` Number of days since today.
#'   Files older than this date will be deleted. If `file_age` = 0L,
#'   files from the current day will be also included.
#'
#' @export
clearCache = function(
    cache_dir = getOption("ZENTRACLOUD_CACHE_DIR")
    , file_age = getOption("ZENTRACLOUD_CACHE_MAX_AGE")
) {

  stopifnot(
    "The provided directory does not exist!"
    = {!is.null(cache_dir) && dir.exists(cache_dir)}
  )

  stopifnot(
    "`file_age` must be an integer of length one"
    = {is.integer(file_age) & length(file_age) == 1L}
  )


  rmFile(
    pth = cache_dir
    , del = file_age
  )

  # remove newly empty directories

  dirs = list.dirs(
    cache_dir
    , full.names = TRUE
    , recursive = FALSE
  )

  d = lapply(
    seq_along(
      dirs
    )
    , function(x) {
      # level sensor
      drs = list.dirs(
        dirs[[x]]
        , recursive = FALSE
        , full.names = TRUE
      )

      lapply(
        seq_along(
          drs
        )
        , function(z) {

          # level year
          ds = list.dirs(
            drs[[z]]
            , recursive = FALSE
            , full.names = TRUE
          )

          # remove all empty year folders
          lapply(
            seq_along(
              ds
            )
            , function(y) {

              fls = list.files(
                ds[y]
                , all.files = TRUE
                , recursive = TRUE
                , full.names = TRUE
                , include.dirs = TRUE
              )

              if (length(fls) == 0) {
                unlink(
                  ds[[y]]
                  , recursive = TRUE
                )
              }
            }
          )

          # level sensor
          fls = list.files(
            drs[[z]]
            , all.files = TRUE
            , recursive = TRUE
            , full.names = TRUE
            , include.dirs = TRUE
          )

          # delete empty sensor folders
          if (length(fls) == 0) {
            unlink(
              drs[[z]]
              , recursive = TRUE
            )
          }

        }
      )


      # level device_sn
      fls = list.files(
        dirs[[x]]
        , all.files = TRUE
        , recursive = TRUE
        , full.names = TRUE
        , include.dirs = TRUE
      )

      # delete empty device_sn folders
      if (length(fls) == 0) {
        unlink(
          dirs[[x]]
          , recursive = TRUE
        )
      }
    }
  )
  invisible(d)
}


#' Read all Data from Cache
#'
#' Load all files that are saved in the cache.
#'
#' @param cache_dir `character` Cache folder. Defaults to the cache directory
#'   stored in options, but can be changed if files were saved somewhere else.
#'   MUST be the highest level of the cache to run properly.
#'
#' @importFrom arrow open_dataset
#' @importFrom dplyr all_of arrange select collect
#'
#' @return
#' A list with entries for each device that was found and all associated data,
#' sorted by logger.
#'
#' @examples
#' \dontrun{
#' all_cached = readCache(cache_dir = getOption("ZENTRACLOUD_CACHE_DIR"))
#' }
#'
#' @export
readCache = function(
  cache_dir = getOption("ZENTRACLOUD_CACHE_DIR")
  ) {

  # define globals to avoid check notes
  datetime = NULL

  # check existence of cache dir
  if (!dir.exists(cache_dir)) {
    stop(
      "`cache_dir` does not exist."
      , call. = FALSE
    )
  }

  # list all directories
  dirs = list.dirs(cache_dir, recursive = FALSE)

  if (length(dirs) == 0) {
    stop(
      message = "No data in cache."
      , call. = FALSE
    )
  }

  # read data
  out = lapply(
    seq_along(dirs)
    , function(y) {
      # read parquet
      pq = arrow::open_dataset(
        dirs[[y]]
        , unify_schemas = TRUE
      )

      fls = pq$files
      fls = lapply(
        seq_along(fls)
        , function(x) {
          fl = sub("/Year.*", "", fls[[x]])
        }
      )

      fls = unique(fls)

      # exclude date month from returned df
      excl = c(
        "Year"
        , "Month"
      )

      dat = lapply(
        seq_along(fls)
        , function(x) {

          pq = arrow::open_dataset(
            fls[[x]]
            , unify_schemas = TRUE
          )

          pq |>
            dplyr::select(-dplyr::all_of(excl)) |>
            dplyr::arrange(datetime) |>
            dplyr::collect()

        }
      )

      nms = sub(".*/sensor=", "", fls)

      out = lapply(
        seq_along(dat)
        , function(x) {
          dat = dat[[x]]
          attr(dat, "sensor") = nms[x]
          return(dat)
        }
      )
      names(out) = nms

      return(out)
    }
  )

  nms = sub(
    ".*(?=.{8}$)"
    , ""
    , dirs
    , perl = TRUE
  )
  names(out) = nms

  return(out)
}






# helper function --------------------------------------------------------------

rmFile = function(
    pth
    , del
) {

  # browser()
  if (length(list.files(pth, pattern = ".parquet")) != 0) {
    fls =
      list.files(
        pth
        , pattern = "*.parquet"
        , full.names = TRUE
      )

    fls_df = data.frame(
      "paths" = fls
      , "mtime" = as.Date(
        file.info(fls)$mtime
      )
    )

    # remove old files
    sapply(
      seq(
        1,
        nrow(fls_df)
        , by = 1
      )
      , function(x) {
        if (fls_df$mtime[x] <= Sys.Date() - del) {
          unlink(
            fls_df$paths[x]
            , recursive = TRUE
          )
        }
      }
    )

    # delete folders if they were emptied
    fldrs = sub(
      '/[^/]*$'
      , ''
      , fls_df$paths[1]
    )
    fls = list.files(
      fldrs
      , all.files = TRUE
      , recursive = TRUE
      , full.names = TRUE
    )

    if (length(fls) == 0) {
      unlink(
        fldrs
        , recursive = TRUE
      )
    }

  } else {

    dirs = list.dirs(
      pth
      , full.names = TRUE
      , recursive = FALSE
    )
    sapply(
      seq_along(
        dirs
      )
      , function(x) {
        rmFile(
          dirs[[x]]
          , del
        )
      }
    )
  }
}



