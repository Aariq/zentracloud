library(listviewer)
library(zentracloud)
library(dygraphs)

log = zentracloud:::queryDeviceReadings(
  user = "tim.appelhans@metergroup.com",
  user_pw = "k4JAHmb0!zc",
  # token = "0495e49957b6db916d4e34cfa35d34bf6710dd8e",
  domain = "",
  device_sn = "z6-01486",
  device_pw = "67365-71244",
  start_time = as.numeric(as.POSIXct("2019-06-05 00:00:00")),
  end_time = as.numeric(as.POSIXct("2019-06-06 23:55:00"))
)

log = zentracloud:::queryDeviceReadings(
  # user = "tim.appelhans@metergroup.com",
  # user_pw = "k4JAHmb0!zc",
  token = "0495e49957b6db916d4e34cfa35d34bf6710dd8e",
  domain = "tahmo",
  device_sn = "06-00708",
  device_pw = "tapkn-kaycs",
  start_time = as.numeric(as.POSIXct("2019-06-05 00:00:00")),
  end_time = as.numeric(as.POSIXct("2019-06-06 23:55:00"))
)

log = zentracloud:::getPortConfiguration(
  # user = "tim.appelhans@metergroup.com",
  # user_pw = "k4JAHmb0!zc",
  token = "0495e49957b6db916d4e34cfa35d34bf6710dd8e",
  domain = "tahmo",
  device_sn = "06-00708",
  device_pw = "tapkn-kaycs",
  start_time = as.numeric(as.POSIXct("2019-06-05 00:00:00")),
  end_time = as.numeric(as.POSIXct("2019-06-06 23:55:00"))
)

log = zentracloud:::getPortConfiguration(
  user = "tim.appelhans@metergroup.com",
  user_pw = "k4JAHmb0!zc",
  # token = "0495e49957b6db916d4e34cfa35d34bf6710dd8e",
  domain = "",
  device_sn = "z6-01486",
  device_pw = "67365-71244",
  start_time = as.numeric(as.POSIXct("2019-06-05 00:00:00")),
  end_time = as.numeric(as.POSIXct("2019-06-06 23:55:00"))
)

log = zentracloud:::queryDeviceStatuses(
  user = "tim.appelhans@metergroup.com",
  user_pw = "k4JAHmb0!zc",
  # token = "0495e49957b6db916d4e34cfa35d34bf6710dd8e",
  domain = "",
  device_sn = "z6-01486",
  device_pw = "67365-71244",
  start_time = as.numeric(as.POSIXct("2019-06-05 00:00:00")),
  end_time = as.numeric(as.POSIXct("2019-06-06 23:55:00"))
)

log = zentracloud:::queryDeviceSettings(
  user = "tim.appelhans@metergroup.com",
  user_pw = "k4JAHmb0!zc",
  # token = "0495e49957b6db916d4e34cfa35d34bf6710dd8e",
  domain = "",
  device_sn = "z6-01486",
  device_pw = "67365-71244"
)

mapDevice(
  user = "tim.appelhans@metergroup.com",
  user_pw = "k4JAHmb0!zc",
  # token = "0495e49957b6db916d4e34cfa35d34bf6710dd8e",
  domain = "",
  device_sn = "z6-01486",
  device_pw = "67365-71244"
)

jsonedit(log)

ifo = getDeviceInfo(log) # list of device information
jsonedit(ifo)

conf = zentracloud:::getLoggerConfig(log)
jsonedit(conf)

(vars = zentracloud:::getVars(conf))

vals = zentracloud:::extractValuesFromLoggerConfig(conf, 3)

extractVarNames(conf)
extractVarValues(conf)

system.time({
  dat = getDeviceData(
    user = "tim.appelhans@metergroup.com",
    user_pw = "k4JAHmb0!zc",
    # token = "0495e49957b6db916d4e34cfa35d34bf6710dd8e",
    domain = "",
    device_sn = "z6-01486",
    device_pw = "67365-71244",
    start_time = as.POSIXct("2019-05-01 00:00:00"),
    end_time = as.POSIXct("2019-05-31 23:55:00"),
    port = 1,
    tz = "local"
  )
})


system.time({
  dat = getDeviceData(
    # user = "tim.appelhans@metergroup.com",
    # user_pw = "k4JAHmb0!zc",
    token = "0495e49957b6db916d4e34cfa35d34bf6710dd8e",
    domain = "tahmo",
    device_sn = "06-00708",
    device_pw = "tapkn-kaycs",
    start_time = as.POSIXct("2019-05-01 00:00:00"),
    end_time = as.POSIXct("2019-05-02 23:55:00"),
    port = 2,
    tz = "local"
  )
})


var = "SOLARRAD"

ts = xts::xts(dat[, c("DATETIME", var)],
              order.by = dat$DATETIME)

dygraph(data = ts) %>%
  dyOptions(useDataTimezone = TRUE) %>%
  dySeries(var)



getTimeSeries(vals)

(sn = getSensorSerials(conf))




r = httr::GET(
  "http://tahmo.zentracloud.com/api/v1/settings?sn=06-00708&token=0495e49957b6db916d4e34cfa35d34bf6710dd8e"
)

tst = httr::POST(
  url = "https://tahmo.zentracloud.com/api/v1",
  config = list(
    user = "ml@zentracloud.com",
    password = "zocdyz-byhso8-purraZ"
  ),
  encode = "json"
)

headers(r)

tst = fromJSON(content(r, "text"), simplifyDataFrame = TRUE, flatten = TRUE)

loc = tst$device$locations

loc_sf = sf::st_as_sf(loc, coords = c("longitude", "latitude"), crs = 4326)

mapview::mapview(loc_sf)
