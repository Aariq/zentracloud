library(listviewer)
library(zentracloud)
library(httr)

dat = zentracloud::getDeviceData(
  user = "ml@zentracloud.com",
  user_pw = "zocdyz-byhso8-purraZ",
  # token = "0495e49957b6db916d4e34cfa35d34bf6710dd8e",
  domain = "tahmo",
  device_sn = "06-00708",
  device_pw = "tapkn-kaycs",
  start_time = "2019-01-01 00:00:00",
  end_time = "2019-01-15 23:55:00"
)

head(dat)

plot(dat$V8 ~ dat$Datetime, type = "l")
plot(dat$V2 ~ dat$Datetime, type = "h")


### Why duplicate description & units for all readings?

readings = zentracloud:::queryDeviceReadings(
  user = "ml@zentracloud.com",
  user_pw = "zocdyz-byhso8-purraZ",
  # token = "0495e49957b6db916d4e34cfa35d34bf6710dd8e",
  domain = "tahmo",
  device_sn = "06-00708",
  device_pw = "tapkn-kaycs",
  start_time = "2019-01-01 00:00:00",
  end_time = "2019-01-01 23:55:00"
)

jsonedit(readings)


### how to get time settings / time zone info?

settings = zentracloud:::queryDeviceSettings(
  user = "ml@zentracloud.com",
  user_pw = "zocdyz-byhso8-purraZ",
  # token = "0495e49957b6db916d4e34cfa35d34bf6710dd8e",
  domain = "tahmo",
  device_sn = "06-00708",
  device_pw = "tapkn-kaycs",
  start_time = "2019-01-01 00:00:00",
  end_time = "2019-02-28 23:55:00"
)

jsonedit(settings)


### how to access API with token?
# st = as.integer(Sys.time() - 3600 * 24)
# nd = as.integer(Sys.time())

# works
usr = httr::GET(
  "http://tahmo.zentracloud.com/api/v1/readings?user=ml@zentracloud.com&user_password=zocdyz-byhso8-purraZ&sn=06-00708&device_password=tapkn-kaycs&start_time=1553526322&end_time=1553612694"
)

usr
content(usr)

# does not work
tok = httr::GET(
  # "http://tahmo.zentracloud.com/api/v1/readings&sn=06-00708&start_time=1546297200&end_time=1548975300",
  # "http://tahmo.zentracloud.com/api/v1/readings?sn=06-00708&start_time=1543526300&end_time=1543612600",
  "http://tahmo.zentracloud.com/api/v1/readings&sn=06-00708&start_time=1543526300&end_time=1543612600",
  add_headers(Authorization = "Token 0495e49957b6db916d4e34cfa35d34bf6710dd8e")
)

tok
content(tok, as = "text")

