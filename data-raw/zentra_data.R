library(zentracloud)

setZentracloudOptions(token = Sys.getenv("ZENTRACLOUD_TOKEN"))

zentra_data = getReadings(
  device_sn = "06-01185"
  , start_time = "2022-06-01 00:00:00"
  , end_time = "2022-06-14 23:59:00"
  , force_api = FALSE
)

# subset
zentra_data = lapply(zentra_data, \(z) dplyr::slice_head(z, n = 20))

usethis::use_data(zentra_data, overwrite = TRUE)

# document
# usethis::use_r("data")
