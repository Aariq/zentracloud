image: meterds/r-cicd-minimal:4.3.0

variables:
  # R CMD CHECK (https://cran.r-project.org/doc/manuals/r-patched/R-ints.html#Tools)
  _R_CHECK_CRAN_INCOMING_: "false"
  _R_CHECK_FORCE_SUGGESTS_: "false"
  # renv
  RENV_PATHS_CACHE: ${CI_PROJECT_DIR}/renv/cache

# https://docs.gitlab.com/ee/ci/caching/
cache:
  # share cache across all branches and all jobs
  key: one-key-to-rule-them-all
  paths:
    - ${RENV_PATHS_CACHE}

stages:
  - test # check, covr
  - build # pkgbuild, pkgdown
  - deploy # release, pages

# anchors
# anchors
.install-sysreqs: &install-sysreqs
  - apt-get -qq update
  - apt-get -qq install --no-install-recommends
      libxt6 libxmu6
      > /dev/null

.renv-restore: &renv-restore
  - Rscript -e "renv::restore()"

.default_rules:
  rules:
    # do not run on merge requests
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
      when: never
    # run on "bump (dev) version" tags
    - if: $CI_COMMIT_TAG =~ /^(\d+\.\d+\.\d+)(\.\d+)?$/
    # run on scheduled pipelines
    - if: $CI_PIPELINE_SOURCE == "schedule"

# jobs

## test stage ----
check:
  stage: test
  rules:
    - !reference [.default_rules, rules]
    # run on all branches (for all kinds of commits)
    - if: $CI_COMMIT_BRANCH
  before_script:
    - *renv-restore
  script:
    - echo "Run R CMD check"
    - Rscript -e "renv::install(
        c('rcmdcheck', 'httptest2', 'testthat')
        )"
    - Rscript -e "rcmdcheck::rcmdcheck(
        quiet = TRUE,
        args = c('--no-manual', '--no-build-vignettes', '--ignore-vignettes'),
        build_args = c('--no-manual', '--no-build-vignettes'),
        error_on = 'warning'
        )"

covr:
  stage: test
  coverage: '/Coverage: \d+\.\d+/'
  rules:
    # do not run on "bump (dev) version" tags
    - if: $CI_COMMIT_TAG =~ /^(\d+\.\d+\.\d+)(\.\d+)?$/
      when: never
    - !reference [.default_rules, rules]
    # run on all branches (for all kinds of commits)
    - if: $CI_COMMIT_BRANCH
  allow_failure: true
  before_script:
    - *renv-restore
  script:
    - echo "use covr for code coverage"
    - mkdir -p coverage
    - Rscript -e "renv::install(
        c('covr', 'DT', 'httptest2', 'testthat', 'xml2')
        )"
    - Rscript -e "cov = covr::package_coverage();
        covr::gitlab(
          coverage = cov,
          file = 'coverage/coverage.html',
          quiet = FALSE
          );
        covr::to_cobertura(
          cov = cov
          , filename = 'cobertura.xml'
        )"
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - coverage
    reports:
      coverage_report:
        coverage_format: cobertura
        path: cobertura.xml

## build stage ----
pkgbuild:
  stage: build
  rules:
    - !reference [.default_rules, rules]
  allow_failure: false
  before_script:
    - *renv-restore
    - mkdir package
  script:
    - echo "use pkgbuild to create single tar.gz bundle"
    - Rscript -e "pkgbuild::build(
        dest_path = 'package',
        vignettes = FALSE
      )"
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - package
    expire_in: 30 days

pkgdown:
  stage: build
  rules:
    - !reference [.default_rules, rules]
  allow_failure: true
  before_script:
    - *install-sysreqs
    - *renv-restore
  script:
    - echo "use pkgdown to generate site"
    - Rscript -e "renv::install(c('pkgdown'))"
    - Rscript -e "pkgdown::build_site(
        override = list(
          template = list(bootstrap = 5),
          news = list(cran_dates = FALSE),
          footer = list(
            structure = list(left = 'mds'),
            components = list(mds = 'Developed by [METER Group Inc](https://www.metergroup.com)')
            )
          )
      )"
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - docs
    expire_in: 30 days

# deploy stage ----
release:
  cache: {}
  stage: deploy
  rules:
    - !reference [.default_rules, rules]
  dependencies:
    - pkgbuild
  allow_failure: false
  script:
    - mkdir -p release
    - cp -r package/* release
    - ls -lth release
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - release
    expire_in: 30 days

pages:
  cache: {}
  stage: deploy
  rules:
    - !reference [.default_rules, rules]
  dependencies:
    - pkgdown
  allow_failure: true
  script:
    - mkdir -p public
    - cp -r docs/* public
    - ls -lth public
  artifacts:
    name: "$CI_JOB_NAME"
    paths:
      - public
    expire_in: 30 days
